/* global G711Session, G726Session, AACSession, cloneArray */

'use strict';

importScripts(
    'Decode/audio.js'
);

addEventListener('message', receiveMessage, false);

var audioRtpSessionsArray = [];
var sdpInfo = null;
var rtpSession = null;
var isBackupCommand = false;

function receiveMessage(event) {
    var message = event.data;

    switch (message.type) {
        case 'sdpInfo':
            sdpInfo = message.data.sdpInfo;
            var aacCodecInfo = message.data.aacCodecInfo;
            setAudioRtpSession(sdpInfo, aacCodecInfo);
            break;
        case 'MediaData':
            //debug.log(message.data.rtspInterleave + '   '  + message.data.payload)
            var channelID = message.data.rtspInterleave[1];
            if (typeof audioRtpSessionsArray[channelID] !== 'undefined') {
                var rtpdata = message.data;
                var frameData = audioRtpSessionsArray[channelID].parseRTPData(rtpdata.rtspInterleave,
                    rtpdata.payload, isBackupCommand, message.info);

                if (frameData !== null && typeof frameData !== 'undefined' &&
                    frameData.streamData !== null && typeof frameData.streamData !== 'undefined') {
                    frameData.streamData = null;
                }
                sendMessage('render', frameData);

            }
            break;
        default:
            break;
    }
}

function setAudioRtpSession(sdpInfo, aacCodecInfo) {
    var SDPInfo = sdpInfo;

    for (var sdpIndex = 0; sdpIndex < sdpInfo.length; sdpIndex++) {
        if (SDPInfo[sdpIndex].trackID.search('trackID=t') === -1) {
            rtpSession = null;
            switch (SDPInfo[sdpIndex].codecName) {
                case 'G.711A':
                case 'G.711Mu':
                    rtpSession = new G711Session(SDPInfo[sdpIndex].codecName);
                    rtpSession.setCodecInfo(SDPInfo[sdpIndex]);
                    break;
                case 'G.726-16':
                case 'G.726-24':
                case 'G.726-32':
                case 'G.726-40':
                    var bit = parseInt(SDPInfo[sdpIndex].codecName.substr(6, 2));
                    debug.log(bit)
                    rtpSession = new G726Session(bit);
                    break;
                case 'mpeg4-generic':
                    rtpSession = new AACSession();
                    debug.log('aacCodecInfo:  ' + JSON.stringify(aacCodecInfo))
                    rtpSession.setCodecInfo(aacCodecInfo);
                    break;
            }

            var channelID = SDPInfo[sdpIndex].RtpInterlevedID;
            audioRtpSessionsArray[channelID] = rtpSession;

            //暂只支持一路音频
            if (rtpSession != null){
                return;
            }
        }
    }
}

function sendMessage(type, data) {
    var event = {
        'type': type,
        'codec': data.codec,
        'data': data.bufferData,
        'rtpTimeStamp': data.rtpTimeStamp,
        'samplingRate': data.samplingRate || 8000
    };

    if (type === 'render') {
        postMessage(event, [data.bufferData.buffer]);
    } else if (type === 'backup') {
        var backupMessage = {
            'type': type,
            'data': data,
        };
        postMessage(backupMessage);
    } else {
        postMessage(event);
    }
}