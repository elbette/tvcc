/*
 MP4封装类
 */
var MP4Remux = function () {
    var _types = [];
    var _dtsBase;
    var datas = {};

    _types = {
        avc1: [], avcC: [], btrt: [], dinf: [],
        dref: [], esds: [], ftyp: [], hdlr: [],
        mdat: [], mdhd: [], mdia: [], mfhd: [],
        minf: [], moof: [], moov: [], mp4a: [],
        mvex: [], mvhd: [], sdtp: [], stbl: [],
        stco: [], stsc: [], stsd: [], stsz: [],
        stts: [], tfdt: [], tfhd: [], traf: [],
        trak: [], trun: [], trex: [], tkhd: [],
        vmhd: [], smhd: []
    };

    function Constructor () {
        for (var name in _types) {
            _types[name] = [
                name.charCodeAt(0),
                name.charCodeAt(1),
                name.charCodeAt(2),
                name.charCodeAt(3)
            ];
        }

        _dtsBase = 0;

        datas.FTYP = new Uint8Array([
            0x69, 0x73, 0x6F, 0x6D, // major_brand: isom
            0x0,  0x0,  0x0,  0x1,  // minor_version: 0x01
            0x69, 0x73, 0x6F, 0x6D, // isom
            0x61, 0x76, 0x63, 0x31  // avc1
        ]);

        datas.STSD_PREFIX = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x01  // entry_count
        ]);

        datas.STTS = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x00  // entry_count
        ]);

        datas.STSC = datas.STCO = datas.STTS;

        datas.STSZ = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x00, // sample_size
            0x00, 0x00, 0x00, 0x00  // sample_count
        ]);

        datas.HDLR_VIDEO = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x00, // pre_defined
            0x76, 0x69, 0x64, 0x65, // handler_type: 'vide'
            0x00, 0x00, 0x00, 0x00, // reserved: 3 * 4 bytes
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x56, 0x69, 0x64, 0x65,
            0x6F, 0x48, 0x61, 0x6E,
            0x64, 0x6C, 0x65, 0x72, 0x00 // name: VideoHandler
        ]);

        datas.HDLR_AUDIO = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x00, // pre_defined
            0x73, 0x6F, 0x75, 0x6E, // handler_type: 'soun'
            0x00, 0x00, 0x00, 0x00, // reserved: 3 * 4 bytes
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x53, 0x6F, 0x75, 0x6E,
            0x64, 0x48, 0x61, 0x6E,
            0x64, 0x6C, 0x65, 0x72, 0x00 // name: SoundHandler
        ]);

        datas.DREF = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x01, // entry_count
            0x00, 0x00, 0x00, 0x0C, // entry_size
            0x75, 0x72, 0x6C, 0x20, // type 'url '
            0x00, 0x00, 0x00, 0x01  // version(0) + flags
        ]);

        // Sound media header
        datas.SMHD = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version(0) + flags
            0x00, 0x00, 0x00, 0x00  // balance(2) + reserved(2)
        ]);

        // video media header
        datas.VMHD = new Uint8Array([
            0x00, 0x00, 0x00, 0x01, // version(0) + flags
            0x00, 0x00,             // graphicsmode: 2 bytes
            0x00, 0x00, 0x00, 0x00, // opcolor: 3 * 2 bytes
            0x00, 0x00
        ]);
    }

    var box = function (type){
        var size = 8;
        var arrs = Array.prototype.slice.call(arguments, 1);
        for (var i = 0; i < arrs.length; i++) {
            size += arrs[i].byteLength;
        }

        var data = new Uint8Array(size);
        var pos = 0;

        // set size
        data[pos++] = size >>> 24 & 0xFF;
        data[pos++] = size >>> 16 & 0xFF;
        data[pos++] = size >>>  8 & 0xFF;
        data[pos++] = size & 0xFF;

        // set type
        data.set(type, pos);
        pos += 4;

        // set data
        for (var i = 0; i < arrs.length; i++) {
            data.set(arrs[i], pos);
            pos += arrs[i].byteLength;
        }

        return data;
    };

    //组装mp4a
    var esds = function(meta) {
        var config = meta.config;
        var configSize = config.length;
        var data = new Uint8Array([
            0x00, 0x00, 0x00, 0x00, // version 0 + flags

            0x03,                   // descriptor_type
            0x17 + configSize,      // length3
            0x00, 0x01,             // es_id
            0x00,                   // stream_priority

            0x04,                   // descriptor_type
            0x0F + configSize,      // length
            0x40,                   // codec: mpeg4_audio
            0x15,                   // stream_type: Audio
            0x00, 0x00, 0x00,       // buffer_size
            0x00, 0x00, 0x00, 0x00, // maxBitrate
            0x00, 0x00, 0x00, 0x00, // avgBitrate

            0x05                    // descriptor_type
        ].concat(
            [configSize]
        ).concat(
            config
        ).concat(
            [0x06, 0x01, 0x02]      // GASpecificConfig
        ));

        return box(_types.esds, data);
    };

    //组装stsd
    var audioSample = function(track) {
        return box(_types.mp4a, new Uint8Array([

            // SampleEntry, ISO/IEC 14496-12
            0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, // reserved
            0x00, 0x01, // data_reference_index

            // AudioSampleEntry, ISO/IEC 14496-12
            0x00, 0x00, 0x00, 0x00, // reserved
            0x00, 0x00, 0x00, 0x00, // reserved
            (track.channelcount & 0xff00) >> 8,
            (track.channelcount & 0xff), // channelcount

            (track.samplesize & 0xff00) >> 8,
            (track.samplesize & 0xff), // samplesize
            0x00, 0x00, // pre_defined
            0x00, 0x00, // reserved

            (track.samplerate & 0xff00) >> 8,
            (track.samplerate & 0xff),
            0x00, 0x00 // samplerate, 16.16

            // MP4AudioSampleEntry, ISO/IEC 14496-14
        ]), esds(track));
    };
    var videoSample = function(track) {
        var sps = track.sps || [],
            pps = track.pps || [],
            sequenceParameterSets = [],
            pictureParameterSets = [],
            i = 0;

        // assemble the SPSs
        for (i = 0; i < sps.length; i++) {
            sequenceParameterSets.push((sps[i].byteLength & 0xFF00) >>> 8);
            sequenceParameterSets.push((sps[i].byteLength & 0xFF)); // sequenceParameterSetLength
            sequenceParameterSets = sequenceParameterSets.concat(Array.prototype.slice.call(sps[i])); // SPS
        }

        // assemble the PPSs
        for (i = 0; i < pps.length; i++) {
            pictureParameterSets.push((pps[i].byteLength & 0xFF00) >>> 8);
            pictureParameterSets.push((pps[i].byteLength & 0xFF));
            pictureParameterSets = pictureParameterSets.concat(Array.prototype.slice.call(pps[i]));
        }

        return box(_types.avc1, new Uint8Array([
            0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, // reserved
            0x00, 0x01, // data_reference_index
            0x00, 0x00, // pre_defined
            0x00, 0x00, // reserved
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, // pre_defined
            (track.width & 0xff00) >> 8,
            track.width & 0xff, // width
            (track.height & 0xff00) >> 8,
            track.height & 0xff, // height
            0x00, 0x48, 0x00, 0x00, // horizresolution
            0x00, 0x48, 0x00, 0x00, // vertresolution
            0x00, 0x00, 0x00, 0x00, // reserved
            0x00, 0x01, // frame_count
            0x13,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, // compressorname
            0x00, 0x18, // depth = 24
            0x11, 0x11 // pre_defined = -1
        ]), box(_types.avcC, new Uint8Array([
            0x01, // configurationVersion
            track.profileIdc, // AVCProfileIndication
            track.profileCompatibility, // profile_compatibility
            track.levelIdc, // AVCLevelIndication
            0xff // lengthSizeMinusOne, hard-coded to 4 bytes
        ].concat([
            sps.length // numOfSequenceParameterSets
        ]).concat(sequenceParameterSets).concat([
            pps.length // numOfPictureParameterSets
        ]).concat(pictureParameterSets))));
    };

    //组装stbl
    var stsd = function(meta) {
        if (meta.type === 'audio') {
            return box(_types.stsd, datas.STSD_PREFIX, audioSample(meta));
        } else {
            return box(_types.stsd, datas.STSD_PREFIX, videoSample(meta));
        }
    };

    //组装minf
    var dinf = function() {
        return box(_types.dinf, box(_types.dref, datas.DREF));
    };
    var stbl = function(meta) {
        var result = box(_types.stbl,   // type: stbl
            stsd(meta),                   // Sample Description Table
            box(_types.stts, datas.STTS), // Time-To-Sample
            box(_types.stsc, datas.STSC), // Sample-To-Chunk
            box(_types.stsz, datas.STSZ), // Sample size
            box(_types.stco, datas.STCO)  // Chunk offset
        );

        return result;
    };

    //组装mdia
    var mdhd = function(meta) {
        var timescale = meta.timescale;
        var duration = meta.duration;

        return box(_types.mdhd, new Uint8Array([
            0x00, 0x00, 0x00, 0x00,    // version(0) + flags
            0x00, 0x00, 0x00, 0x00,    // creation_time
            0x00, 0x00, 0x00, 0x00,    // modification_time
            (timescale >>> 24) & 0xFF, // timescale: 4 bytes
            (timescale >>> 16) & 0xFF,
            (timescale >>>  8) & 0xFF,
            (timescale) & 0xFF,
            (duration >>> 24) & 0xFF,  // duration: 4 bytes
            (duration >>> 16) & 0xFF,
            (duration >>>  8) & 0xFF,
            (duration) & 0xFF,
            0x55, 0xC4,                // language: und (undetermined)
            0x00, 0x00                 // pre_defined = 0
        ]));
    };
    var hdlr = function(meta) {
        var data = null;

        if (meta.type === 'audio') {
            data = datas.HDLR_AUDIO;
        } else {
            data = datas.HDLR_VIDEO;
        }
        return box(_types.hdlr, data);
    };
    var minf = function(meta) {
        var xmhd = null;
        if (meta.type === 'audio') {
            xmhd = box(_types.smhd, datas.SMHD);
        } else {
            xmhd = box(_types.vmhd, datas.VMHD);
        }
        return box(_types.minf, xmhd, dinf(), stbl(meta));
    };


    // 组装trak
    var tkhd = function(meta) {
        var trackId = meta.id;
        var duration = meta.duration;
        var width = meta.width;
        var height = meta.height;
        return box(_types.tkhd, new Uint8Array([
            0x00, 0x00, 0x00, 0x07,   // version(0) + flags
            0x00, 0x00, 0x00, 0x00,   // creation_time
            0x00, 0x00, 0x00, 0x00,   // modification_time
            (trackId >>> 24) & 0xFF,  // track_ID: 4 bytes
            (trackId >>> 16) & 0xFF,
            (trackId >>>  8) & 0xFF,
            (trackId) & 0xFF,
            0x00, 0x00, 0x00, 0x00,   // reserved: 4 bytes
            (duration >>> 24) & 0xFF, // duration: 4 bytes
            (duration >>> 16) & 0xFF,
            (duration >>>  8) & 0xFF,
            (duration) & 0xFF,
            0x00, 0x00, 0x00, 0x00,   // reserved: 2 * 4 bytes
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,   // layer(2bytes) + alternate_group(2bytes)
            0x00, 0x00, 0x00, 0x00,   // volume(2bytes) + reserved(2bytes)
            0x00, 0x01, 0x00, 0x00,   // ----begin composition matrix----
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x01, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x40, 0x00, 0x00, 0x00,   // ----end composition matrix----
            (width >>> 8) & 0xFF,     // width and height
            (width) & 0xFF,
            0x00, 0x00,
            (height >>> 8) & 0xFF,
            (height) & 0xFF,
            0x00, 0x00
        ]));
    };
    var mdia = function(meta) {
        return box(_types.mdia, mdhd(meta), hdlr(meta), minf(meta));
    };
    //组装mvex
    var trex = function(meta) {
        var trackId = meta.id;
        var data = new Uint8Array([
            0x00, 0x00, 0x00, 0x00,  // version(0) + flags
            (trackId >>> 24) & 0xFF, // track_ID
            (trackId >>> 16) & 0xFF,
            (trackId >>>  8) & 0xFF,
            (trackId) & 0xFF,
            0x00, 0x00, 0x00, 0x01,  // default_sample_description_index
            0x00, 0x00, 0x00, 0x00,  // default_sample_duration
            0x00, 0x00, 0x00, 0x00,  // default_sample_size
            0x00, 0x01, 0x00, 0x01   // default_sample_flags
        ]);

        return box(_types.trex, data);
    };

    //组装moov
    var mvhd = function(timescale, duration) {
        debug.log('mvhd:  timescale: ' + timescale + '  duration: ' + duration );
        return box(_types.mvhd, new Uint8Array([
            0x00, 0x00, 0x00, 0x00,    // version(0) + flags
            0x00, 0x00, 0x00, 0x00,    // creation_time
            0x00, 0x00, 0x00, 0x00,    // modification_time
            (timescale >>> 24) & 0xFF, // timescale: 4 bytes
            (timescale >>> 16) & 0xFF,
            (timescale >>>  8) & 0xFF,
            (timescale) & 0xFF,
            (duration >>> 24) & 0xFF,  // duration: 4 bytes
            (duration >>> 16) & 0xFF,
            (duration >>>  8) & 0xFF,
            (duration) & 0xFF,
            0x00, 0x01, 0x00, 0x00,    // Preferred rate: 1.0
            0x01, 0x00, 0x00, 0x00,    // PreferredVolume(1.0, 2bytes) + reserved(2bytes)
            0x00, 0x00, 0x00, 0x00,    // reserved: 4 + 4 bytes
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x01, 0x00, 0x00,    // ----begin composition matrix----
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x01, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x40, 0x00, 0x00, 0x00,    // ----end composition matrix----
            0x00, 0x00, 0x00, 0x00,    // ----begin pre_defined 6 * 4 bytes----
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,    // ----end pre_defined 6 * 4 bytes----
            0xFF, 0xFF, 0xFF, 0xFF     // next_track_ID
        ]));
    };
    var trak = function(meta) {
        return box(_types.trak, tkhd(meta), mdia(meta));
    };
    var mvex = function(meta) {
        return box(_types.mvex, trex(meta));
    };

    //组装initSegment
    var moov = function(meta) {
        var mvhd1 = mvhd(meta.timescale, meta.duration);
        var trak1 = trak(meta);
        var mvex1 = mvex(meta);

        return box(_types.moov, mvhd1, trak1, mvex1);
    };


    //组装traf
    var sdtp = function(track) {
        var samples = track.samples || [];
        var sampleCount = samples.length;
        var data = new Uint8Array(4 + sampleCount);

        // 0~4 bytes: version(0) & flags
        for (var i = 0; i < sampleCount; i++) {
            var flags = samples[i].flags;
            data[i + 4] = (flags.isLeading << 6) // is_leading: 2 (bit)
                | (flags.dependsOn << 4)           // sample_depends_on
                | (flags.isDependedOn << 2)        // sample_is_depended_on
                | (flags.hasRedundancy);           // sample_has_redundancy
        }

        return box(_types.sdtp, data);
    };
    //var trun = function(track, offset) {
    //    var samples = track.samples || [];
    //    var sampleCount = samples.length;
    //    var dataSize = 12 + 16 * sampleCount;
    //    var data = new Uint8Array(dataSize);
    //
    //    offset += 8 + dataSize;
    //
    //    data.set([
    //        0x00, 0x00, 0x0F, 0x01,      // version(0) & flags
    //        (sampleCount >>> 24) & 0xFF, // sample_count
    //        (sampleCount >>> 16) & 0xFF,
    //        (sampleCount >>>  8) & 0xFF,
    //        (sampleCount) & 0xFF,
    //        (offset >>> 24) & 0xFF,      // data_offset
    //        (offset >>> 16) & 0xFF,
    //        (offset >>>  8) & 0xFF,
    //        (offset) & 0xFF
    //    ], 0);
    //
    //    for (var i = 0; i < sampleCount; i++) {
    //        var duration = samples[i].duration;
    //        var size = samples[i].size;
    //        var flags = samples[i].flags;
    //        var cts = samples[i].cts;
    //
    //        data.set([
    //            (duration >>> 24) & 0xFF, // sample_duration
    //            (duration >>> 16) & 0xFF,
    //            (duration >>>  8) & 0xFF,
    //            (duration) & 0xFF,
    //            (size >>> 24) & 0xFF,     // sample_size
    //            (size >>> 16) & 0xFF,
    //            (size >>>  8) & 0xFF,
    //            (size) & 0xFF,
    //            (flags.isLeading << 2) | flags.dependsOn, // sample_flags
    //            (flags.isDependedOn << 6) | (flags.hasRedundancy << 4) | flags.isNonSync,
    //            0x00, 0x00,               // sample_degradation_priority
    //            (cts >>> 24) & 0xFF,      // sample_composition_time_offset
    //            (cts >>> 16) & 0xFF,
    //            (cts >>>  8) & 0xFF,
    //            (cts) & 0xFF
    //        ], 12 + 16 * i);
    //    }
    //
    //    return box(_types.trun, data);
    //};
    var trunHeader1 = function (samples, offset) {
        return [0, 0, 3, 5, (samples.length & 4278190080) >>> 24, (samples.length & 16711680) >>> 16, (samples.length & 65280) >>> 8, samples.length & 255, (offset & 4278190080) >>> 24, (offset & 16711680) >>> 16, (offset & 65280) >>> 8, offset & 255, 0, 0, 0, 0]
    };
    var videoTrun = function (track, _offset) {
        var bytes = null, samples = null, sample = null, i = 0;
        var offset = _offset;
        samples = track.samples || [];
        if (samples[0].frameDuration === null) {
            offset += 8 + 12 + 4 + 4 * samples.length;
            bytes = trunHeader(samples, offset);
            for (i = 0; i < samples.length; i++) {
                sample = samples[i];
                bytes = bytes.concat([(sample.size & 4278190080) >>> 24, (sample.size & 16711680) >>> 16, (sample.size & 65280) >>> 8, sample.size & 255])
            }
        } else {
            offset += 8 + 12 + 4 + 4 * samples.length + 4 * samples.length;
            bytes = trunHeader1(samples, offset);
            for (i = 0; i < samples.length; i++) {
                sample = samples[i];
                bytes = bytes.concat([(sample.frameDuration & 4278190080) >>> 24, (sample.frameDuration & 16711680) >>> 16, (sample.frameDuration & 65280) >>> 8, sample.frameDuration & 255, (sample.size & 4278190080) >>> 24, (sample.size & 16711680) >>> 16, (sample.size & 65280) >>> 8, sample.size & 255])
            }
        }
        return box(_types.trun, new Uint8Array(bytes))
    };
    var trun = function (track, offset) {
        if (track.type === "audio") {
            return audioTrun(track, offset)
        }
        return videoTrun(track, offset)
    };

    //组装moof
    var mfhd = function(sequenceNumber) {
        var data = new Uint8Array([
            0x00, 0x00, 0x00, 0x00,
            (sequenceNumber >>> 24) & 0xFF, // sequence_number: int32
            (sequenceNumber >>> 16) & 0xFF,
            (sequenceNumber >>>  8) & 0xFF,
            (sequenceNumber) & 0xFF
        ]);

        return box(_types.mfhd, data);
    };
    //var traf = function(track, baseMediaDecodeTime) {
    //    var trackId = track.id;
    //
    //    // Track fragment header box
    //    var tfhd = box(_types.tfhd, new Uint8Array([
    //        0x00, 0x00, 0x00, 0x00,  // version(0) & flags
    //        (trackId >>> 24) & 0xFF, // track_ID
    //        (trackId >>> 16) & 0xFF,
    //        (trackId >>>  8) & 0xFF,
    //        (trackId) & 0xFF
    //    ]));
    //
    //    // Track Fragment Decode Time
    //    var tfdt = box(_types.tfdt, new Uint8Array([
    //        0x00, 0x00, 0x00, 0x00,              // version(0) & flags
    //        (baseMediaDecodeTime >>> 24) & 0xFF, // baseMediaDecodeTime: int32
    //        (baseMediaDecodeTime >>> 16) & 0xFF,
    //        (baseMediaDecodeTime >>>  8) & 0xFF,
    //        (baseMediaDecodeTime) & 0xFF
    //    ]));
    //
    //    var sdtp1 = sdtp(track);
    //    var trun1 = trun(track, sdtp1.byteLength + 16 + 16 + 8 + 16 + 8 + 8);
    //
    //    return box(_types.traf, tfhd, tfdt, trun1, sdtp1);
    //};
    var traf = function (track) {
        var trackFragmentHeader = null, trackFragmentDecodeTime = null, trackFragmentRun = null, dataOffset = null;
        trackFragmentHeader = box(_types.tfhd, new Uint8Array([0, 2, 0, 0, 0, 0, 0, 1]));
        trackFragmentDecodeTime = box(_types.tfdt, new Uint8Array([
            0, 0, 0, 0,
            track.baseMediaDecodeTime >>> 24 & 255,
            track.baseMediaDecodeTime >>> 16 & 255,
            track.baseMediaDecodeTime >>> 8 & 255,
            track.baseMediaDecodeTime & 255
        ]));
        dataOffset = 16 + 16 + 8 + 16 + 8 + 8;
        trackFragmentRun = trun(track, dataOffset);
        return box(_types.traf, trackFragmentHeader, trackFragmentDecodeTime, trackFragmentRun)
    };

    //组装mediaSegment
    var moof = function(sequenceNumber, track) {
        //console.log('moof--------:  sequenceNumber: ' + JSON.stringify(sequenceNumber) + '  track: ' + JSON.stringify(track))
        return box(_types.moof, mfhd(sequenceNumber), traf(track));
        //var trackFragments = [], i = tracks.length;
        //while (i--) {
        //    trackFragments[i] = traf(tracks[i])
        //}
        //return box.apply(null, [_types.moof, mfhd(sequenceNumber)].concat(trackFragments))
        //console.log('trackFragmentsLength: ' +trackFragments.length)
    };
    var mdat = function(data) {
        return box(_types.mdat, data);
    };

    Constructor.prototype = {
        initSegment: function (meta) {
            var ftyp = box(_types.ftyp, datas.FTYP);
            debug.log(meta)
            var moov1 = moov(meta);

            var seg = new Uint8Array(ftyp.byteLength + moov1.byteLength);
            seg.set(ftyp, 0);
            seg.set(moov1, ftyp.byteLength);

            return seg;
        },
        mediaSegment: function(sequenceNumber, tracks, data, ept) {
            var moofBox = moof(sequenceNumber, tracks);
            var frameData = mdat(data);
            var result = null;

            result = new Uint8Array(moofBox.byteLength + frameData.byteLength);
            result.set(moofBox);
            result.set(frameData, moofBox.byteLength);

            return result;
        }
    };

    return new Constructor;
};

var mp4Remux = new MP4Remux();