/**
 * Created by 33596 on 2018/4/26.
 * 封装对外接口，支持自定义事件(内部通过在workermanager中注册各种回调函数实现)
 */

var PlayerControl = function (options) {
    this.wsURL = options.wsURL;
    this.rtspURL = options.rtspURL;
    this.isPlayback = options.playback || false;
    this.ws = null;
    this.decodeMode = options.decodeMode;
    this.events = {
        ResolutionChanged: function () {},
        PlayStart: function () {},
        DecodeStart: function () {},
        UpdateCanvas: function () {},
        GetFrameRate: function () {},
        FrameTypeChange: function () {},
        Error: function () {},
        MSEResolutionChanged: function () {},
        audioChange: function () {},
        WorkerReady:function(){},
        IvsDraw:function(){},
        FileOver:function(){}
    };

    this.username = options.username;
    this.password = options.password;
};

PlayerControl.prototype = {
    init: function (can, videoElem) {
        //var rtsp = new RTSPServer(this.wsURL, this.rtspURL);
        this.ws = new WebsocketServer(this.wsURL, this.rtspURL);
		console.log("1",this.wsURL,this.rtspURL);
        try {
		this.ws.init(can, videoElem);
		}catch(err) {
			console.log('errore: ' ,err);
		}
		console.log("2");
        this.ws.setLiveMode(this.decodeMode);
		console.log("3");
        this.ws.setUserInfo(this.username, this.password);
		console.log("4");
        this.ws.setPlayMode(this.isPlayback);
		console.log("5");
        //for(var i = 0; i < this.events.length; i++){
        //    this.ws.setCallback(this.events[i].eventName, this.events[i].callback);
        //}
        //this.events = [];
        for(var i in this.events){
            this.ws.setCallback(i, this.events[i]);
        }

        this.events = null;
    },
    connect: function () {
        this.ws.connect();
    },
    play: function () {
        this.controlPlayer('PLAY');
    },
    pause: function () {
        this.controlPlayer('PAUSE');
    },
    stop: function () {
        this.controlPlayer('TEARDOWN');
    },
    close: function () {
        this.ws.disconnect();
    },
    playByTime: function (range) { //回放时才能按时间播放
        this.controlPlayer('PLAY', 'video', range);
    },
    playFF: function (speed) {
        //快放
        this.controlPlayer('PAUSE');
        this.controlPlayer('SCALE', speed);
    },
    playRewind: function () {
        //快退
    },
    audioPlay: function () {
        this.controlPlayer('audioPlay', 'start');
    },
    audioStop: function () {
        this.controlPlayer('audioPlay', 'stop');
    },
    setAudioSamplingRate: function (val) {
        this.controlPlayer('audioSamplingRate', val);
    },
    setAudioVolume: function (val) {
        this.controlPlayer('volumn', val);
    },
    controlPlayer: function (command, data, range) {
        var option;
        if (data === 'video') {
            option = {
                command: command,
                range: range ? range : 0
            };
        }else {
            option = {
                command: command,
                data: data
            }
        }
        this.ws.controlPlayer(option);
    },
    setPlayMode: function (mode) {
        this.ws.setLiveMode(mode);
    },
    setPlayPath: function (rtspURL) {
        this.ws.setRTSPURL(rtspURL);
    },
    capture: function (filename) {
        this.ws.capture(filename);
    },
    /**
     * 注册自定义事件,目前支持如下事件:
     * [ResolutionChanged] 分辨率改变,返回具体分辨率
     * [PlayStart] 播放开始, 指通过canvas开始播放第一帧或者video加载了initsegment，返回值为"PlayStart"
     * [DecodeStart] 开始解码，指通过SPS/DH帧头 获取到第一帧的分辨率，仅canvas可用，返回值为{width:xxx, height: xxx}
     * [UpdateCanvas] 每绘制一帧时触发一次,返回值为{width:xxx, height: xxx}
     * [GetFrameRate] 返回帧率，在SDP信息中获取
     * [FrameTypeChange] 编码类型改变，返回新类型, 支持"MJPEG","H264","H265"
     * [Error] 错误信息，返回值为{errorCode: xxx} ,错误码类型如下
     * *** 101  延时大于两秒 ***
     * *** 201  不支持当前音频格式 ***
     * *** 404  找不到流    ***
     * [MSEResolutionChanged] 由于MSE不能获取分辨率改变的事件，故只能在收到一帧时进行判断
     * [audioChange] 音频改变事件，当音频编码格式改变或者采样率改变时触发
     * @param eventName 事件名
     * @param callback  回调函数
     * @return player
     */
    on: function (eventName, callback) {
        //this.events.push({
        //    eventName: eventName,
        //    callback: callback
        //});

        this.events[eventName] = callback;
       // this.ws.setCallback(eventName, callback);
    }
};