//处理视频数据和辅助帧的子线程
//实例化session并将数据传入
//将session处理后的数据传递到主线程的workermanager
'use strict';
importScripts(
    'Decode/video.js'
);
sendMessage('WorkerReady');
addEventListener('message', receiveMessage, false);

var videoRtpSessionsArray = [];
var sdpInfo = null;
var rtpSession = null;
var decodeMode = "";
var isBackupCommand = false;
var isStepPlay = false;
var isForward = true;
var framerate = 0;
var backupFrameInfo = null;
var videoCHID = -1;
var h264Session = null;
var h265Session = null;
var mjpegSession = null;
var ivsSession = null;
var channelId = null;
var dropout = 1;

function receiveMessage(event) {
    var message = event.data;
    channelId = event.data.channelId;
    switch (message.type) {
        case 'sdpInfo':
            sdpInfo = message.data;
            framerate = 0;
            setVideoRtpSession(sdpInfo);
            break;
        case 'MediaData':
            if (isStepPlay === true) {
                buffering(message);
                break;
            }
            videoCHID = message.data.rtspInterleave[1];
            if (typeof videoRtpSessionsArray[videoCHID] !== "undefined") {
                videoRtpSessionsArray[videoCHID].parseRTPData(message.data.rtspInterleave,
                    message.data.payload, isBackupCommand, dropout, message.info);
            }
            break;
        case 'initStartTime':
            videoRtpSessionsArray[videoCHID].initStartTime();
            break;
        case 'end':
            sendMessage('end');
            break;
        default:
            break;
    }
}

function setVideoRtpSession(data) {
    videoRtpSessionsArray = [];
    isStepPlay = false;
    for (var sdpIndex = 0; sdpIndex < data.sdpInfo.length; sdpIndex++) {
        rtpSession = null;
        decodeMode = data.decodeMode;

        if (data.sdpInfo[sdpIndex].codecName === 'H264') {
            if (h264Session === null) {
                h264Session = H264Session();
            }
            rtpSession = h264Session;
            rtpSession.init(data.decodeMode);
            rtpSession.setFramerate(data.sdpInfo[sdpIndex].Framerate);
            rtpSession.setGovLength(data.govLength);
            rtpSession.setCheckDelay(data.checkDelay);
        } else if (data.sdpInfo[sdpIndex].codecName === 'H265') {
            if (h265Session === null) {
                h265Session = H265Session();
            }
            rtpSession = h265Session;
            rtpSession.init();
            rtpSession.setFramerate(data.sdpInfo[sdpIndex].Framerate);
            rtpSession.setGovLength(data.govLength);
            rtpSession.setCheckDelay(data.checkDelay);
        } else if (data.sdpInfo[sdpIndex].codecName === 'JPEG') {
            if (mjpegSession === null) {
                mjpegSession = MjpegSession();
            }
            rtpSession = mjpegSession;
            rtpSession.init();
            rtpSession.setFramerate(data.sdpInfo[sdpIndex].Framerate);
        }else if (data.sdpInfo[sdpIndex].codecName === 'stream-assist-frame') {
            debug.log(data.sdpInfo[sdpIndex])
            if(ivsSession === null) {
                ivsSession = IvsSession();
            }
            rtpSession = ivsSession;
            rtpSession.init();
        }

        if (typeof data.sdpInfo[sdpIndex].Framerate !== "undefined") {
            framerate = data.sdpInfo[sdpIndex].Framerate;
        }
        if (rtpSession !== null) {
            rtpSession.setBufferfullCallback(BufferFullCallback);
            rtpSession.setReturnCallback(RtpReturnCallback);
            videoCHID = data.sdpInfo[sdpIndex].RtpInterlevedID;
            videoRtpSessionsArray[videoCHID] = rtpSession;
        }
    }
}

function buffering(message) {
    videoCHID = message.data.rtspInterleave[1];
    if (typeof videoRtpSessionsArray[videoCHID] !== "undefined") {
        videoRtpSessionsArray[videoCHID].bufferingRtpData(message.data.rtspInterleave,
            message.data.header, message.data.payload);
    }
}

function BufferFullCallback() {
    videoRtpSessionsArray[videoCHID].findCurrent();
    sendMessage('stepPlay', 'BufferFull');
}

function RtpReturnCallback(dataInfo) {
    var mediaData = null;
    var backupData = null;

    if (dataInfo === null || typeof dataInfo === "undefined") {
        mediaData = null;
        backupData = null;
        return;
    } else if (typeof dataInfo.error !== "undefined") {
        sendMessage('error', dataInfo.error);
        mediaData = dataInfo.decodedData;
        //return;
    } else {
        mediaData = dataInfo.decodedData;
        if (dataInfo.decodeMode !== null && typeof dataInfo.decodeMode !== "undefined") {
            decodeMode = dataInfo.decodeMode;
            sendMessage('setVideoTagMode', dataInfo.decodeMode);
        }
        //if (dataInfo.backupData !== null && typeof dataInfo.backupData !== "undefined") {
        //    backupData = cloneArray(dataInfo.backupData.stream);
        //    dataInfo.backupData.stream = null;
        //}
        //if (dataInfo.throughPut !== null && typeof dataInfo.throughPut !== "undefined") {
        //    sendMessage('throughPut', dataInfo.throughPut);
        //}
    }
    if (dataInfo.decodeStart != null) {
        sendMessage('DecodeStart', dataInfo.decodeStart)
        decodeMode = dataInfo.decodeStart.decodeMode;
    }
    if (mediaData !== null && typeof mediaData !== "undefined") {
        //if (mediaData.playback !== null && typeof mediaData.playback !== "undefined") {
        //    sendMessage('playbackFlag', mediaData.playback);
        //}
        if (mediaData.frameData !== null && decodeMode === "canvas") {
            if (mediaData.frameData.firstFrame === true) {
                sendMessage('firstFrame', mediaData.frameData.firstFrame);
                //return;
            }
            var frameInfo = {
                'bufferIdx': mediaData.frameData.bufferIdx,
                'width': mediaData.frameData.width,
                'height': mediaData.frameData.height,
                'codecType': mediaData.frameData.codecType,
                'frameType': mediaData.frameData.frameType,
                'timeStamp': null,
            };
            if (mediaData.timeStamp !== null && typeof mediaData.timeStamp !== "undefined") {
                frameInfo.timeStamp = mediaData.timeStamp;
            }
            sendMessage('videoInfo', frameInfo);

            if (typeof mediaData.frameData.data !== "undefined" && mediaData.frameData.data !== null) {
                sendMessage('canvasRender', mediaData.frameData.data);
            }
        } else if (mediaData.frameData !== null && decodeMode === "video") {
            if (mediaData.initSegmentData !== null) {
                sendMessage('codecInfo', mediaData.codecInfo);
                sendMessage('initSegment', mediaData.initSegmentData);
            }
            var frameInfo = {
                'codecType': mediaData.frameData.codecType
            };
            if (typeof mediaData.frameData.width !== "undefined") {
                frameInfo.width = mediaData.frameData.width;
                frameInfo.height = mediaData.frameData.height;
            }
            sendMessage('videoInfo', frameInfo);

            sendMessage('videoTimeStamp', mediaData.timeStamp);
            if (mediaData.frameData.length > 0) {
                sendMessage('mediaSample', mediaData.mediaSample);
                sendMessage('videoRender', mediaData.frameData);
            }
        } else {
            sendMessage('drop', dataInfo.decodedData);
        }
    }
    if (dataInfo.resolution != null){
        sendMessage('MSEResolutionChanged', dataInfo.resolution)
    }


    if(dataInfo.ivsDraw != null) {
        sendMessage('ivsDraw', dataInfo)
    }

}

function sendMessage(type, data) {
    var event = {
        'type': type,
        'data': data,
        'channelId': channelId,
    };

    if (type === 'canvasRender') {
        postMessage(event, [data.buffer]);
    } else {
        postMessage(event);
    }
}

function VideoBufferList() {
    var MAX_LENGTH = 0,
        BUFFERING = 0,
        bufferFullCallback = null;

    function Constructor() {
        MAX_LENGTH = 360;
        BUFFERING = 240;
        bufferFullCallback = null;
        this._length = 0;
        this.head = null;
        this.tail = null;
        this.curIdx = 0
    }

    Constructor.prototype = {
        push: function (data, width, height, codecType, frameType, timeStamp) {
            var node = new VideoBufferNode(data, width, height, codecType, frameType, timeStamp);
            if (this._length > 0) {
                this.tail.next = node;
                node.previous = this.tail;
                this.tail = node;
            } else {
                this.head = node;
                this.tail = node;
            }
            this._length += 1;

            (bufferFullCallback !== null && this._length >= BUFFERING) ? bufferFullCallback() : 0; //PLAYBACK bufferFull
            //      debug.log("VideoBufferList after push node count is " + this._length + " frameType is " + frameType);

            return node;
        },
        pop: function () {
            //    debug.log("before pop node count is " + this._length + " MINBUFFER is " + MINBUFFER);
            var node = null;
            if (this._length > 1) {
                node = this.head;
                this.head = this.head.next;
                if (this.head !== null) {
                    this.head.previous = null;
                    // 2nd use-case: there is no second node
                } else {
                    this.tail = null;
                }
                this._length -= 1;
            }
            return node;
        },
        setMaxLength: function (length) {
            MAX_LENGTH = length;
            if (MAX_LENGTH > 360) {
                MAX_LENGTH = 360;
            } else if (MAX_LENGTH < 30) {
                MAX_LENGTH = 30;
            }
        },
        setBUFFERING: function (interval) {
            BUFFERING = interval;
            if (BUFFERING > 240) {
                BUFFERING = 240;
            } else if (BUFFERING < 6) {
                BUFFERING = 6;
            }
        },
        setBufferFullCallback: function (callback) {
            bufferFullCallback = callback;
            // debug.log("setBufferFullCallback MAX_LENGTH is " + MAX_LENGTH );
        },
        searchTimestamp: function (frameTimestamp) {
            //      debug.log("searchTimestamp frameTimestamp = " + frameTimestamp.timestamp + " frameTimestamp usec = " + frameTimestamp.timestamp_usec);
            var currentNode = this.head,
                length = this._length,
                count = 1,
                message = {
                    failure: 'Failure: non-existent node in this list.'
                };

            // 1st use-case: an invalid position
            if (length === 0 || frameTimestamp <= 0 || currentNode === null) {
                throw new Error(message.failure);
            }

            // 2nd use-case: a valid position
            while (currentNode !== null &&
            (currentNode.timeStamp.timestamp !== frameTimestamp.timestamp ||
            currentNode.timeStamp.timestamp_usec !== frameTimestamp.timestamp_usec)) {
                //        debug.log("currentNode Timestamp = " + currentNode.timeStamp.timestamp + " Timestamp usec = " + currentNode.timeStamp.timestamp_usec);
                currentNode = currentNode.next;
                count++;
            }

            if (length < count) {
                currentNode = null;
            } else {
                this.curIdx = count;
                // debug.log("searchTimestamp curIdx = " + this.curIdx + " currentNode.timeStamp.timestamp = " + currentNode.timeStamp.timestamp + " currentNode.timestamp_usec = " + currentNode.timeStamp.timestamp_usec + " frameTimestamp = " + frameTimestamp.timestamp + " frameTimestamp usec = " + frameTimestamp.timestamp_usec);
            }

            return currentNode;
        },
        findIFrame: function (isForward) {
            var currentNode = this.head,
                length = this._length,
                count = 1,
                message = {
                    failure: 'Failure: non-existent node in this list.'
                };

            // 1st use-case: an invalid position
            if (length === 0) {
                throw new Error(message.failure);
            }

            // 2nd use-case: a valid position
            while (count < this.curIdx) {
                currentNode = currentNode.next;
                count++;
            }

            if (isForward === true) {
                while (currentNode.frameType !== "I") {
                    currentNode = currentNode.next;
                    count++;
                }
            } else {
                while (currentNode.frameType !== "I") {
                    currentNode = currentNode.previous;
                    count--;
                }
            }

            if (length < count) {
                currentNode = null;
            } else {
                this.curIdx = count;
                // debug.log('findIFrame curIdx ' + this.curIdx + ' count ' + count + ' _length ' + this._length);
            }

            return currentNode;
        }
    };
    return new Constructor();
}