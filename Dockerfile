FROM node:12-alpine

WORKDIR /usr/src/app

RUN npm install -g http-server

COPY src/ ./

EXPOSE 8080
CMD [ "http-server"]